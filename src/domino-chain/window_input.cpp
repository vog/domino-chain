/* Domino-Chain
 *
 * Domino-Chain is the legal property of its developers, whose
 * names are listed in the AUTHORS file, which is included
 * within the source distribution.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335 USA
 */

#include "window.h"

#include "colors.h"
#include "screen.h"
#include "graphicsn.h"

#include <sstream>


class InputWindow_c : public window_c {

  private:
    std::string input;

    unsigned int cursorPosition;
    unsigned int textstart; // first character to be displayed (for long strings
    unsigned int textlen; // number of characters to display

    std::string title;
    std::string subtitle;

    void redraw(void);

  public:

    InputWindow_c(int x, int y, int w, int h, surface_c & s, graphicsN_c & gr,
        const std::string & title, const std::string & subtitle, const std::string & init = "");

    // the the user has selected something
    const std::string getText(void) { return input; } // which list entry was selected

    virtual bool handleEvent(const SDL_Event & event);
};


InputWindow_c::InputWindow_c(int x, int y, int w, int h, surface_c & s, graphicsN_c & gr,
        const std::string & ti, const std::string & suti, const std::string & txt) : window_c(x, y, w, h, s, gr)
{
  title = ti;
  subtitle = suti;
  input = txt;
  cursorPosition = txt.length();
  textstart = 0;
  textlen = txt.length();
  escape = false;
  redraw();
}



bool InputWindow_c::handleEvent(const SDL_Event & event)
{
  if (window_c::handleEvent(event)) return true;

  if (event.type == SDL_KEYDOWN)
  {
    if (event.key.keysym.sym == SDLK_ESCAPE)
    {
      escape = true;
      done = true;
      return true;
    }
    else if (event.key.keysym.sym == SDLK_RETURN)
    {
      done = true;
      return true;
    }
    else if (event.key.keysym.sym == SDLK_LEFT)
    {
      if (cursorPosition > 0) {
        cursorPosition--;
        while (   (cursorPosition > 0)
               && ((input[cursorPosition] & 0xC0) == 0x80)
              )
        {
          cursorPosition--;
        }
        redraw();
      }
      return true;
    }
    else if (event.key.keysym.sym == SDLK_RIGHT)
    {
      if (cursorPosition+1 <= input.length()) {
        cursorPosition++;
        while (   (cursorPosition < input.length())
               && ((input[cursorPosition] & 0xC0) == 0x80)
              )
        {
          cursorPosition++;
        }
        redraw();
      }
      return true;
    }
    else if (event.key.keysym.sym == SDLK_HOME)
    {
        if (cursorPosition > 0)
        {
          cursorPosition = 0;
          redraw();
        }
    }
    else if (event.key.keysym.sym == SDLK_END)
    {
        if (cursorPosition+1 < input.length())
        {
          cursorPosition = input.length();
          redraw();
        }
    }
    else if (event.key.keysym.sym == SDLK_BACKSPACE)
    {
      while (   (cursorPosition > 0)
             && ((input[cursorPosition-1] & 0xC0) == 0x80)
            )
      {
        input.erase(cursorPosition-1, 1);
        cursorPosition--;
      }
      if (cursorPosition > 0)
      {
        input.erase(cursorPosition-1, 1);
        cursorPosition--;
      }
      redraw();
    }
    else if (event.key.keysym.sym == SDLK_DELETE)
    {
      if (cursorPosition < input.length())
      {
        input.erase(cursorPosition, 1);

        while (   (cursorPosition < input.length())
               && ((input[cursorPosition] & 0xC0) == 0x80)
              )
        {
          input.erase(cursorPosition, 1);
        }
        redraw();
      }
    }
  }
  else if (event.type == SDL_TEXTINPUT)
  {
    int p = 0;
    while (event.text.text[p])
    {
      input.insert(cursorPosition, 1, event.text.text[p]);
      cursorPosition++;
      p++;
    }
    redraw();
  }

  return false;
}

void InputWindow_c::redraw(void)
{
  clearInside();

  fontParams_s par;

  par.font = FNT_BIG;
  par.alignment = ALN_TEXT_CENTER;
  par.color.r = TXT_COL_R; par.color.g = TXT_COL_G; par.color.b = TXT_COL_B;
  par.shadow = 2;
  par.box.x = gr.blockX()*(x+1);
  par.box.y = gr.blockY()*(y+1);
  par.box.w = gr.blockX()*(w-2);
  par.box.h = getFontHeight(FNT_BIG);

  surf.renderText(&par, title);

  int ypos = gr.blockY()*(y+1) + getTextHeight(&par, title);

  surf.fillRect(gr.blockX()*(x+1)+1, ypos+1, gr.blockX()*(w-2), 2, 0, 0, 0);
  surf.fillRect(gr.blockX()*(x+1), ypos, gr.blockX()*(w-2), 2, TXT_COL_R, TXT_COL_G, TXT_COL_B);

  ypos += 20;

  if (subtitle != "")
  {
    par.font = FNT_SMALL;
    par.shadow = false;
    par.box.y = ypos;
    surf.renderText(&par, subtitle);
    ypos += getTextHeight(&par, subtitle);
  }

  par.alignment = ALN_TEXT;
  par.font = FNT_NORMAL;
  par.shadow = false;

  if (textstart > cursorPosition) textstart = cursorPosition;

  unsigned int wi = getTextWidth(FNT_NORMAL, input.substr(textstart, cursorPosition-textstart));

  if (wi > gr.blockX()*(w-2))
  {
    while (wi > gr.blockX()*(w-2) && (textstart < cursorPosition))
    {
      textstart++;
      while (   (textstart < cursorPosition)
          && ((input[textstart] & 0xC0) == 0x80)
          )
      {
        textstart++;
      }

      wi = getTextWidth(FNT_NORMAL, input.substr(textstart, cursorPosition-textstart));
    }
  }

  textlen = input.length()-textstart;

  unsigned int txtwi = getTextWidth(FNT_NORMAL, input.substr(textstart, textlen));

  while (txtwi > gr.blockX()*(w-2) && (textlen > 0))
  {
    textlen--;
    while (   (textlen > 0)
        && ((input[textstart+textlen] & 0xC0) == 0x80)
        )
    {
      textlen--;
    }
    txtwi = getTextWidth(FNT_NORMAL, input.substr(textstart, textlen));
  }

  surf.fillRect(gr.blockX()*(x+1)+wi, ypos, 4, getFontHeight(FNT_NORMAL), 0, 0, 0);

  par.box.y = ypos;
  surf.renderText(&par, input.substr(textstart, textlen));
}


window_c * getProfileInputWindow(surface_c & surf, graphicsN_c & gr)
{
  return new InputWindow_c(4,2,12,6, surf, gr, _("Enter new profile name"), _("ATTENTION: Changing the name is not possible later on, so choose wisely."));
}

window_c * getNewLevelWindow(surface_c & surf, graphicsN_c & gr)
{
  return new InputWindow_c(4,2,12,6, surf, gr, _("Enter file name for the new level"), _("Please use only basic latin letters (a-z), numbers and the dash and underscore."));
}

window_c * getAuthorsAddWindow(surface_c & surf, graphicsN_c & gr)
{
  return new InputWindow_c(4,2,12,5, surf, gr, _("Enter new authors name"), "");
}

window_c * getLevelnameWindow(surface_c & surf, graphicsN_c & gr, const std::string & init)
{
  return new InputWindow_c(4,2,12,5, surf, gr, _("Enter new level name"), _("When the level name is descriptive, please use English as language."), init);
}

window_c * getTimeWindow(surface_c & surf, graphicsN_c & gr, int time)
{
  int min = time / 60;
  int sec = time % 60;

  std::ostringstream timeStream;

  timeStream << min << ":" << sec;

  return new InputWindow_c(4,2,12,5, surf, gr, _("Enter time for level"), _("Enter as 'minutes:seconds'."), timeStream.str());
}

window_c * getHintWindow(surface_c & surf, graphicsN_c & gr, const std::string & h)
{
  return new InputWindow_c(2,2,14,5, surf, gr, _("Enter hint for level"), _("Please use English texts if you intend to publish the level."), h);
}

